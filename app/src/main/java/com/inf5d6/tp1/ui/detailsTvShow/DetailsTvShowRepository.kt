package com.inf5d6.tp1.ui.detailsTvShow

import android.app.Application
import android.content.ContentValues.TAG
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.inf5d6.tp1.MainActivity
import com.inf5d6.tp1.models.DetailsTvShow

class DetailsTvShowRepository (private val application: Application) {

    fun getHead(): MutableMap<String, String> {
        val headerMap = mutableMapOf<String, String>()
        headerMap.put("Content-Type", "application/json");
        headerMap.put("Authorization", "Bearer ${MainActivity.TOKEN.value}");
        return headerMap
    }

    fun getFavorite(favorite: MutableLiveData<Boolean>, IdTvShow: Int){
        val queue = Volley.newRequestQueue(application)

        val r = object : JsonObjectRequest(
            Request.Method.GET,
            MainActivity.SRVURL+"/favorite?tvshowId="+IdTvShow,
            null,
            {
                favorite.value = it.get("isFavorite") as Boolean
                Log.e(TAG, it.get("isFavorite").toString())
            },
            {
                println("ERREUR: /api/favorite?tvshowId")
            })
        {
            override fun getHeaders(): MutableMap<String, String> {
                return getHead()
            }
        }
        queue.add(r)
    }

    fun deleteFavorite( IdTvShow: Int){
        val queue = Volley.newRequestQueue(application)

        val r = object : JsonObjectRequest(
            Request.Method.DELETE,
            MainActivity.SRVURL+"/favorite?tvshowId="+IdTvShow,
            null,
            {
                Toast.makeText(application, "La télésérie a été supprimée des favoris", Toast.LENGTH_LONG).show()            },
            {
                println("ERREUR: /api/favorite?tvshowId")
            })
        {
            override fun getHeaders(): MutableMap<String, String> {
                return getHead()
            }
        }
        queue.add(r)
    }

    fun postFavorite( IdTvShow: Int){
        val queue = Volley.newRequestQueue(application)

        val r = object : JsonObjectRequest(
            Request.Method.POST,
            MainActivity.SRVURL+"/favorite?tvshowId="+IdTvShow,
            null,
            {
                Toast.makeText(application, "La télésérie a été ajoutée aux favoris", Toast.LENGTH_LONG).show()            },
            {
                println("ERREUR: /api/favorite?tvshowId")
            })
        {
            override fun getHeaders(): MutableMap<String, String> {
                return getHead()
            }
        }
        queue.add(r)
    }

    fun getDetailsTvShows(Details: MutableLiveData<DetailsTvShow>, IdTvShow: Int) {
        val queue = Volley.newRequestQueue(application)

        val r = StringRequest(
            Request.Method.GET,
            MainActivity.SRVURL+"/tvshow?tvshowId="+IdTvShow,
            {
                val tvshow = Gson().fromJson(it, DetailsTvShow::class.java)
                Details.value = tvshow
            },
            {
                println("ERREUR: /api/favorite?tvshowId")
            })

        queue.add(r)
    }
}