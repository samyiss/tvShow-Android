package com.inf5d6.tp1.ui.detailsTvShow

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class DetailsViewModelFactory(val application: Application, val IdTvShow: Int) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DetailsTvShowViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return DetailsTvShowViewModel(this.application, IdTvShow) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}