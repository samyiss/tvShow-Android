package com.inf5d6.tp1.ui.favorites

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.inf5d6.tp1.MainActivity
import com.inf5d6.tp1.models.TvShow

class FavoritesRepository(private val application: Application) {
    fun getTvFavorites(favorites: MutableLiveData<MutableList<TvShow>>) {
        val queue = Volley.newRequestQueue(application)

        val r = object : StringRequest(
            Request.Method.GET,
            MainActivity.SRVURL+"/favorites",
            {
                val arrayfavorites = Gson().fromJson(it, Array<TvShow>::class.java)
                favorites.value = arrayfavorites.toMutableList()
            },
            {
                println("ERREUR: /api/favorites")
            })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headerMap = mutableMapOf<String, String>()
                headerMap.put("Content-Type", "application/json");
                headerMap.put("Authorization", "Bearer ${MainActivity.TOKEN.value}");
                return headerMap
            }
        }
        queue.add(r)
    }
}