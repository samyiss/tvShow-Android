package com.inf5d6.tp1.ui.detailsTvShow

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.inf5d6.tp1.R
import com.inf5d6.tp1.adapters.CastRecyclerViewAdapter
import com.inf5d6.tp1.adapters.SeasonRecyclerViewAdapter
import com.squareup.picasso.Picasso


class DetailsTvShowFragment : Fragment() {

    private lateinit var rvListeCast: RecyclerView
    private lateinit var rvListeSeason: RecyclerView


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detailtvshow, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val IdTvShow = this.requireArguments().getString("IdTvShow")!!.toInt()

        var DetailsViewModelFactory = DetailsViewModelFactory(this.requireActivity().application, IdTvShow)
        var DetailsTvShowViewModel = ViewModelProvider(this, DetailsViewModelFactory).get(DetailsTvShowViewModel::class.java)


        this.rvListeCast = view.findViewById(R.id.recycCast)
        this.rvListeCast.layoutManager = LinearLayoutManager(view.context, LinearLayoutManager.HORIZONTAL, false)

        this.rvListeSeason = view.findViewById(R.id.recycSeason)
        this.rvListeSeason.layoutManager = LinearLayoutManager(view.context, LinearLayoutManager.HORIZONTAL, false)

        var favFalse = view.findViewById<ImageView>(R.id.FavoriteFalse)

        DetailsTvShowViewModel.getFavorite(IdTvShow)
        DetailsTvShowViewModel.favorite.observe(viewLifecycleOwner) {
            if(DetailsTvShowViewModel.favorite.value == true) {
                favFalse.setImageResource(android.R.drawable.btn_star_big_on)
                favFalse.setOnClickListener {
                    DetailsTvShowViewModel.deleteFavorite(IdTvShow)
                }
            }
            else if(DetailsTvShowViewModel.favorite.value == false) {
                favFalse.setImageResource(android.R.drawable.btn_star_big_off)
                favFalse.setOnClickListener {
                    DetailsTvShowViewModel.postFavorite(IdTvShow)
                }
            }
            DetailsTvShowViewModel.getFavorite(IdTvShow)
        }

        DetailsTvShowViewModel.tvShow.observe(viewLifecycleOwner) {
            view.findViewById<TextView>(R.id.titre).text = it.title
            view.findViewById<TextView>(R.id.episodes).text = it.episodeCount.toString() + " episodes"
            view.findViewById<TextView>(R.id.annee).text = it.year.toString()
            view.findViewById<TextView>(R.id.plot).text = it.plot
            val imgURL = view.findViewById<ImageView>(R.id.imageURL)
            Picasso.get().load(it.imgURL).into(imgURL)
            this.rvListeCast.adapter = CastRecyclerViewAdapter(it.roles)
            this.rvListeSeason.adapter = SeasonRecyclerViewAdapter(it.seasons)
        }
    }
}