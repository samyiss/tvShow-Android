package com.inf5d6.tp1.ui.favorites

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.inf5d6.tp1.models.TvShow
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Suppress("RedundantVisibilityModifier")
class FavoritesViewModel(val app: Application) : AndroidViewModel(app) {
    public var favorites: MutableLiveData<MutableList<TvShow>> = MutableLiveData(mutableListOf())

    fun getFavorites() {
        val favoritesRepository = FavoritesRepository(getApplication())
        favoritesRepository.getTvFavorites(favorites)
    }

    init {
        viewModelScope.launch(Dispatchers.IO) {
            getFavorites()
        }
    }
}
