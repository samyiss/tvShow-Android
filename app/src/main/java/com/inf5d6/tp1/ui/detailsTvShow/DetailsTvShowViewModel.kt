package com.inf5d6.tp1.ui.detailsTvShow

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.inf5d6.tp1.models.DetailsTvShow
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Suppress("RedundantVisibilityModifier")
class DetailsTvShowViewModel (val app: Application, val IdTvShow: Int) : AndroidViewModel(app) {
    public var tvShow: MutableLiveData<DetailsTvShow> = MutableLiveData()
    public var favorite: MutableLiveData<Boolean> = MutableLiveData()


    fun getFavorite(IdTvShow: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val detailsTvShowRepository = DetailsTvShowRepository(getApplication())
            detailsTvShowRepository.getFavorite(favorite, IdTvShow)
        }
    }

    fun postFavorite(IdTvShow: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val detailsTvShowRepository = DetailsTvShowRepository(getApplication())
            detailsTvShowRepository.postFavorite(IdTvShow)
        }
    }

    fun deleteFavorite(IdTvShow: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val detailsTvShowRepository = DetailsTvShowRepository(getApplication())
            detailsTvShowRepository.deleteFavorite(IdTvShow)
        }
    }

    init {
        viewModelScope.launch(Dispatchers.IO) {
            val DetailsRepository = DetailsTvShowRepository(getApplication())
            DetailsRepository.getDetailsTvShows(tvShow, IdTvShow)
        }
    }
}