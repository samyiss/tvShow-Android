package com.inf5d6.tp1

import android.app.Application
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject

class LoginRepository (private val application: Application) {
    fun postLogin(username: String, password: String) {
        val queue = Volley.newRequestQueue(application)

        val body = JSONObject()
        body.put("username", username)
        body.put("password", password)

        val r = JsonObjectRequest(
            Request.Method.POST,
            MainActivity.SRVURL+"/auth/token",
            body,
            {
                MainActivity.TOKEN.value = it.getString("token")
            },
            {
                Toast.makeText(application, "échec de la connexion veuillez réessayer", Toast.LENGTH_LONG).show()
            }
        )
        queue.add(r)
    }
}