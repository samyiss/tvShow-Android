package com.inf5d6.tp1

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

class LoginActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        var login = this.findViewById<Button>(R.id.login)

        login.setOnClickListener {

            var username = findViewById<EditText>(R.id.username).text.toString()
            var password = findViewById<EditText>(R.id.password).text.toString()

            LoginViewModel(application).loginUser(username, password)

            MainActivity.TOKEN.observe(this){
                var intent = Intent(this, MainActivity::class.java)
                this.startActivity(intent)
            }
        }
    }
}