package com.inf5d6.tp1

import android.app.Application
import androidx.lifecycle.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LoginViewModel(val app: Application) : AndroidViewModel(app) {

    fun loginUser(username: String, password: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val loginRepository = LoginRepository(getApplication())
            loginRepository.postLogin(username, password)
        }
    }
}


