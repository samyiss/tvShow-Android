package com.inf5d6.tp1.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.inf5d6.tp1.R
import com.inf5d6.tp1.models.Role
import com.squareup.picasso.Picasso

class CastRecyclerViewAdapter (private val listCast: List<Role>) :
    RecyclerView.Adapter<CastRecyclerViewAdapter.CastViewHolder>() {

    class CastViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CastViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.actor_item, parent, false) as View
        return CastViewHolder(view)
    }

    override fun onBindViewHolder(holder: CastViewHolder, position: Int) {
        val imgAlbum = holder.view.findViewById<ImageView>(R.id.imgCast)
        Picasso.get().load(this.listCast[position].imgURL).into(imgAlbum)
        holder.view.findViewById<TextView>(R.id.nomCast).text = this.listCast[position].name
        holder.view.findViewById<TextView>(R.id.nomPerso).text = this.listCast[position].character
    }

    override fun getItemCount() = this.listCast.size
}