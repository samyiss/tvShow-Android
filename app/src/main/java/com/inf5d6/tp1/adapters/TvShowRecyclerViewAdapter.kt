package com.inf5d6.tp1.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.inf5d6.tp1.R
import com.inf5d6.tp1.models.TvShow
import com.squareup.picasso.Picasso

class TvShowRecyclerViewAdapter(private val listeTvShow: MutableList<TvShow>, private val details: View) :
    RecyclerView.Adapter<TvShowRecyclerViewAdapter.TvShowViewHolder>() {

    class TvShowViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TvShowViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.tvshow_item, parent, false) as View
        return TvShowViewHolder(view)
    }

    override fun onBindViewHolder(holder: TvShowViewHolder, position: Int) {

        val imgAlbum = holder.view.findViewById<ImageView>(R.id.tvshowURL)
        Picasso.get().load(this.listeTvShow[position].imgURL).into(imgAlbum)

        holder.view.setOnClickListener {
            val id = this.listeTvShow[position].tvshowId.toString()
            val param = bundleOf(Pair("IdTvShow", id))
            details.findNavController().navigate(R.id.fragment_details, param)
        }
    }

    override fun getItemCount() = this.listeTvShow.size
}