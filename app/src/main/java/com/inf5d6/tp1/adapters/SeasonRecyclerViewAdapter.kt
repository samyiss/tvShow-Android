package com.inf5d6.tp1.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.inf5d6.tp1.R
import com.inf5d6.tp1.models.Season
import com.squareup.picasso.Picasso

class SeasonRecyclerViewAdapter (private val listCast: List<Season>) :
    RecyclerView.Adapter<SeasonRecyclerViewAdapter.SeasonViewHolder>() {

    class SeasonViewHolder (val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeasonViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.season_item, parent, false) as View
        return SeasonViewHolder(view)
    }

    override fun onBindViewHolder(holder: SeasonViewHolder, position: Int) {
        val imgAlbum = holder.view.findViewById<ImageView>(R.id.imgSeason)
        Picasso.get().load(this.listCast[position].imgURL).into(imgAlbum)
        holder.view.findViewById<TextView>(R.id.season).text = "Season " + this.listCast[position].number.toString()
        holder.view.findViewById<TextView>(R.id.SeasonEpisode).text = this.listCast[position].episodeCount.toString() + " episodes"
    }

    override fun getItemCount() = this.listCast.size
}